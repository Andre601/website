---
template: frontpage.html

hide:
  - toc
---

## Welcome

Welcome to my personal website!  
On this site can you find links to some of my more popular projects, as-well as my personal blog where I post random stuff.

## Transparency

The contents of my website (Everything found under the main `andre601.ch` domain) are open source and can be found on [:simple-codeberg: Codeberg](https://codeberg.org/Andre601/website){ target="_blank" rel="nofollow" }.  
The Website is hosted and served through [:simple-codeberg: Codeberg Pages](https://codeberg.page){ target="_blank" rel="nofollow" }.

## Links

<div class="grid cards" markdown>

-   ### :octicons-book-24: Blog
    
    ----
    
    My personal blog where I publish all kinds of posts of various topics.
    
    [:octicons-chevron-right-16: Go to Page](blog/index.md)

-   ### :octicons-list-unordered-24: AdvancedServerList
    
    ----
    
    A highly configurable plugin for Paper, BungeeCord, Waterfall and Velocity, that allows you to customize the MOTD, favicon and player count of your server in a player's multiplayer server list.
    
    [:octicons-chevron-right-16: Go to Page](https://asl.andre601.ch){ target="_blank" }

-   ### :octicons-dependabot-24: \*Purr\*
    
    ----
    
    A Discord Bot created with JDA that is focused on fun and entertainment.
    
    [:octicons-chevron-right-16: Go to Page](https://purrbot.site){ target="_blank" rel="noopener" }

-   ### :octicons-image-24: ImageAPI
    
    ----
    
    An open-source API made as a replacement for other common APIs such as nekos.life.  
    Offers various endpoints to get random images from.
    
    [:octicons-chevron-right-16: Go to Page](https://docs.purrbot.site/api){ target="_blank" rel="noopener" }

-   ### :material-language-java: Javadocs
    
    ----

    You can find Javadocs related to some of my projects under my JD subdomain.

    [:octicons-chevron-right-16: Go to Page](https://jd.andre601.ch){ target="_blank" rel="noopener" }

</div>
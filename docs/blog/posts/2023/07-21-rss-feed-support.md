---
title: RSS Feed support
description: My blog should now have RSS Feed support, which also should allow automatic posting of toots for new Blog posts like this one!

date: 2023-07-21

authors:
  - andre601

categories:
  - General

tags:
  - Misc

mastodon_host: 'blobfox.coffee'
mastodon_user: 'andre_601'
comment_id: '110753503585079084'

footnotes: false
---

I've implemented support for an RSS Feed to my blog using the MkDocs RSS Plugin.

With this, there should also now be automatic posting of these blog posts as new mastodon toots.  
This (hopefully) improves and speeds up my process of creating Mastodon toots for comments on my blog.

<!-- more -->

/// info | Update
The automatic posting unfortunately doesn't work as the tool used didn't recognize new posts as such since the page build and site build dates where too different.
///
---
title: New Comment System
description: I switched the comment system of my blog away from Giscus to a Mastodon-based one for a future migration to Codeberg.

date: 2023-07-17

authors:
  - andre601

categories:
  - General

tags:
  - Misc
  - Mastodon

mastodon_host: 'blobfox.coffee'
mastodon_user: 'andre_601'
comment_id: '110730118323340864'

footnotes: false
---

With this post am I announcing two things:

1. The usage of a new comment system
2. The future migration to Codeberg pages

<!-- more -->

## New Comment system

My blog now uses a new Comment system that uses Mastodon posts at its core.

While this has some downsides (i.e. I have to make the Post on Mastodon first before releasing this one to have the post id), does it also bring some nice improvements.  
For example is there no requirement for a GitHub Account and people can use whichever Mastodon-compatible account there is to leave a comment.

I feel like this is a much more open system to use. And it's especially useful since I no longer need to rely on GitHub, making a move away from it much more clean and easy.

## Move to Codeberg pages

This post also announces my move over to Codeberg pages.

I already moved my [personal website](https://andre601.ch) over to it and this Blog should follow hopefully soon, as I feel like Codeberg offers a lot more freedom than what GitHub gives with GitHub pages... Especially in terms of custom domain support.

I cannot tell when or how fast this migration will happen, but it eventually will happen.
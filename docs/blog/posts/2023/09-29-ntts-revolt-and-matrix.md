---
title: NoTextToSpeech's comparison of Revolt and Matrix
description: Why the video of NoTextToSpeech about Revolt and Matrix is awful.

date: 2023-09-29

authors:
  - andre601

categories:
  - Critique

tags:
  - Misc

mastodon_host: 'blobfox.coffee'
mastodon_user: 'andre_601'
comment_id: '111148848937518943'
---

[^1]: https://www.youtube.com/@NoTextToSpeech
[^2]: https://discord.com
[^3]: https://www.youtube.com/watch?v=47EjvPIhAOU
[^4]: https://revolt.chat
[^5]: https://element.io
[^6]: https://matrix.org
[^7]: https://matrix.org/about/#governance

It has been quite a while since the YouTuber NoTextToSpeech made his video regarding more privacy friendly Discord-Alternatives, comparing Revolt and Matrix (or more accurately the element client) with each other.  
This video still bothers me to this day and I can't sit here without giving a more in-depth rant on why this video is not only bad, but also clearly biased towards Matrix, while painting Revolt in a bad light.

<!-- more -->

## Basic info first

Let's first give some quick information so that everyone is up to date with what I will be talking about here.  
NoTextToSpeech[^1], which I will refer to as NTTS in this post from now on, is a YouTuber, mostly making videos related to Discord[^2] where he often points out either bad practices from Discord or exploits, scams and alike that can either be a potential risk, or just plain stupid.

In one of his former videos, namely "Nerdy Privacy-Focused Discord Alternatives"[^3] did he do a comparison of Revolt.chat[^4] and Element[^5] in various categories such as privacy and features.  
This... was an awful watch. The points talked about are fair, but the reasons he gives for why one is better than the other is bad to say the least, as he leaves out important context that should influence the ratings he gave on these topics. It also gave a clear bias of his towards one of the platforms... And let me tell you, it wasn't Revolt.

Now, you may be asking "Why do you care so much? This is months old!" to which I say: Because context is important.  
You can't just go and compare two things with each other that usually don't have anything in common, or only have very surface level similarities to compare. And given his video has half a million views, chances are high it still reaches some and gives the wrong impression to people that Element is superior when it isn't.

But first let us finish the info dump. 
 
Revolt, or Revolt.chat, is a Text and VOIP platform similar to Discord, offering a lot of Discord's features but for free. The platform is open-source and maintained by a group of volunteers.

Then there is Element. Element is a client and server for the matrix protocol standard[^6], which is developed by the Matrix-Foundation, a non-profit UK Community Interest Company[^7]. Element is from what I gathered also managed and/or supported by the Matrix team. NTTS focused on the client in his video.

Also, as a final note will I point out that I'm NOT part of the Revolt team. I don't work on it nor have I been part of it in the past. I just randomly use it and that's it.

## My main problems with the video

### Comparing a fun project with a non-profit

Time to go into detail about what I dislike in NTTS's video. The most important problem I have here with NTTS's comparison is, that one is centralized and the other is decentralized.  
Revolt is by nature a centralized solution, meaning that one server, one domain manages the users, servers, messages, etc. created on it. There is no communication with outside instances of Revolt, no way for a user to host their own instance while still communicating with users from other platforms.

Or to give a more easy example: Imagine Revolt as Twitter and Element as Mastodon. Both offer similar features, but have core differences. In addition is one platform managed by one single instance governing everything, while the other is self-hostable while still connected to others to build a network.

Basically, NTTS compares an apple with a banana here.

The next issue I see here is, that Revolt is managed by volunteers. The people behind it are not a company or similar. They do not get funds outside of donations. They don't have dedicated teams working on the infrastructure and code.  
It's managed by people who go to work, have school and a real life.

Meanwhile Element is, if the info I got is correct, managed by Matrix, which is a non-profic company, meaning they do have dedicated staff paid for working on the Matrix protocol and probs Element itself.

The fact that Revolt lacks polish or features is often explained by it still being worked on by like 2 or 3 devs which do it in their spare free time.

### Privacy != Do whatever

NTTS gives an argument that Revolt doesn't have no end-to-end (e2e) encryption, meaning they (and potentially anyone gaining access to the database) can look at your messages. He also adds a very tone-death example of "not being able to share a bomb-recipe" which I'm sure I don't need to explain why you can't do that even with e2e, but I will anyways.  
E2e is not a "Get out of Jail free" card here. Even if your messages are encrypted, it doesn't automatically allow you to do illegal actions here. Additionally is there, again, a difference between a centralized and decentralized platform.  
Revolt is centralized, and therefore bound to whatever government they associate themself with, which in this case is within the EU and therefore bound to specific, strict laws related to data protection and things you're allowed to do.

Meanwhile a decentralized platform can put parts of their liability away, by allowing you to self-host at which point it's bound to where you life and host your instance. This however, is still no free pass to write whatever you want anywhere.

### Bots aren't part of Revolt

Rating Revolt a 1/5 for "Bot support" is insulting. Similar to Discord are bots on Revolt run and managed by individual devs who may or may not work on Revolt itself, so rating Revolt badly because not too many people make big bots on it and because a Bot may not have too much to offer is stupid. This is not an issue with Revolt. It's an issue with the Bot and how much the dev puts into it to work.

## False information

There is also some false information I want to clear up.  
First he mentions about joining servers and how it requires you to put the invite URL in the browser: No. That is wrong. Posting an invite in chat has the same effect like in discord, meaning you get a modal that has basic info of the server and a join button in it.

NTTS also displays a dashboard for the AutoMod bot and calls it minimal. What he forgets to mention is, that this is the dashboard of AutoMod, which is on a separate site. It's not part of Revolt, and should therefore not be compared to it, yet he displays it in a way that makes you assume it's part of Revolt. I mean... why is it so zoomed in? It couldn't be to hide the domain, exposing it not be official part of revolt, right? Oh wait! He later does show the URL, but doesn't bother pointing out the dashboard is 3rd-party.

## Missed information

NTTS also missed some crucial points I want to point out here.

First thing to mention is that in element e2e is not automatically enabled, at least for public rooms/chats. This is clearly visible in his video with the "End-to-end encryption isn't enabled" message shown, yet he doesn't point it out and it gives the idea that e2e encryption is enabled everywhere, which is a false impression.

Finally he also misses that Revolt has an API, allowing devs to create their own clients for it, making designs however they like. It just isn't promoted or listed clearly like Matrix does.

## Clear Bias

There is some clear bias towards Element at the end of his video, most prominently shown with what features he wish to have in Discord...  
Especially the Beta feature thing which A) Revolt has too in the form of experiments and B) Discord also has through their Canary and PTB versions, which have early versions of Discord with possibly buggy changes and features.

## Conclusion

The point of this post is obviously not to attack NTTS, but to point out the false info and clear bias he had at times during his video, ignoring and not pointing out important context that should change his ratings based on it.  
You can't expect a centralized, open-source software managed by random people to compare well to a decentralized, open-source platform managed by a non-profit company.
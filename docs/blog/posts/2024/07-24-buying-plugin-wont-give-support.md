---
title: Why buying plugins won't give you a right for support
description: Many people have the assumption that because they purchased a plugin, that they have the right for getting support. This is not true and this post will clear things up.

date: 2024-07-24

authors:
  - andre601

categories:
  - Minecraft

tags:
  - Minecraft

mastodon_host: 'blobfox.coffee'
mastodon_user: 'andre_601'
comment_id: '112843975239864570'

footnotes: false
---

# Why buying plugins won't give you a right for support

I provide support for the Premium Plugin ItemsAdder. And throughout my time doing this have I encountered lines like these:

- "Can you finally add support for x?"
- "I bought this plugin, so why won't my issue be solved?"
- "No update in ... did the dev abandon the project?"

Some of these lines may sound extreme, but I've encountered them before at least once. And not just with ItemsAdder, but also other (often times not even premium) plugins.  
And they all have one thing in common most of the time: The user thinks they have a right for support, because they bought (or simply use) the plugin. This couldn't be further from the truth.

<!-- more -->

## The Claims

When buying a plugin, no matter the platform, you as the customer have barely any rights for anything. Yet, people think they do and often times give arguments, like the ones I will list and cover below.

### We pay for the devs living

People think that selling plugins is an easy way for a dev to pay their bills. This couldn't be further from the truth.  
Unless they sell a ridicolous amount each day/week/month, or have a huge number of premium plugins, selling them would barely scratch the surface of the costs they often have when making these plugins.

There is a reason a freelance dev usually won't go for a sallary of 3 figures, let alone 4, but usually around 5 figures at lowest. Programming is a time consuming task and may not be easy, but this isn't for this topic at hand, so I'll skip it.

Many devs I talked with have confirmed to me, that selling plugins doesn't even pay a single bill they usually have per month, so they often times have a real life job they work at, getting the actual money in. Selling plugins is a bonus for their pocket, nothing more.

Such claims that plugin selling gives you a sustainable life usually comes from dellusional people, or people who have other motives behind such a claim, such as Kangarko, who spits out such claims to get you to buy his awful, useless courses, to line his own pockets with.  
I guarantee you, he won't earn enough from his plugins, but from idiots who fall for his claims, buying the course he constantly promotes everywhere.

### They sell plugins, so they have a business and must give support

Not sure where this logic comes from, but no. Creating and selling plugins somewhere does not result in a dev suddenly having a business. By this logic would every dev selling premium plugins on SpigotMC be considered a business.  
One argument that was given against this logic is the simple fact that some offer their plugin for free as an Open-Source project... But that is simply a weak excuse at best, because by such a logic would [all these commercial OS projects](https://en.wikipedia.org/wiki/List_of_commercial_open-source_applications_and_services){ target="_blank" rel="nofollow" } not be considered by a business, yet they mostly are.  
To be a business is not just selling closed-sourced products, but a lot more, like actually being a registered company.

Finally, even if someone selling plugins was a business does it not guarantee you support. No business is required to provide you with any level of support and can easily refuse it. Software can be given "as-is" without guarantee for anything.  
In fact, buying a resource on SpigotMC gives you a infobox with a red text stating exactly that: That there is no guarantee for the plugin being updated, having support, etc.

### They can just hire more devs!

This is another false assumption based on the first claim, that the dev makes enough money.  
Developers aren't cheap. As mentioned before does a competent dev not accept any sallary that would be below 5 figures. Maybe 4 figures if they are nice. But if you want a decent dev, you have to pay a lot. A noob-dev is not someone you want if your plugin is complex and does things not common in plugin development.

Next is there the issue of trust. If your resource is closed-source, sharing it with a dev is risky as they could simply take and leak it, or worse, sell it.  
Finally could such a dev also simply take the money and bail out, leaving the other dev with a dent in their pocket and frustration.

Hiring developers is a nice idea, but with the time it would require to teach them your coding practices, setting up decent communication, the overall costs, and risks mentioned before is this not a reliable solution, nor a good one, especially if you're proud of your own project.

## What can I do then?

Be patient.  
Plugin development is a time-consuming, tedious task. Devs usually don't have all the time in the world to work on their project, be it having a job, going to school/college, or something else.  
And just having the plugin open source is also not an easy solution, because it would require people to actually provide own time to contribute changes needed. These changes would then require proper testing and reviewing by the dev to ensure it works.

So, if you can't contribute code yourself, either due to lack of experience/knowledge or simply because there is no source to contribute to, be patient. Unless the dev clearly states that they won't continue work on a project, will they eventually look at its issues, PRs, etc. and try to fix them. But even then don't assume this is a granted thing. A dev's work on a project is never granted.

A plugin being updated is a priviledge, not a right.
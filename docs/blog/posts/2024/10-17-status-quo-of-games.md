---
title: The Status Quo of Games
description: Talking about current bad practices in Games and why accepting them as given is a bad thing.

date: 2024-10-17

authors:
  - andre601

categories:
  - General

tags:
  - Misc

comment_id: '113368490333097008'
---

# The Status Quo of Games

The term "Status Quo" in its most basic description means "The current state" of something.  
Many things have a Status Quo the community accepts and/or tolerates, including Games.

I want to take the time and talk about why some of the things accepted as the overall norm is not a good idea, nor a good excuse for others to implement.

<!-- more -->

## How this Started

I got the idea of making this post, after watching a video regarding why you shouldn't use the Mod Essential[^1] (I myself made a [blog post] talking about why it isn't a good thing to use).

In the comments of this video have I seen a bunch of replies, justifying things like the cosmetics store with arguments such as "Other games have this too!". This not only ignores the main point - that this store is possibly EULA breaking - but is a weak excuse for a system used that itself is disliked if not even hated by the community.  
So after this did I decide to write this blog post.

## What are current Status Quos?

There are a few Status quos that can be found not only in games, but society itself. One such example could be taxes. It's taken as a common that you have to pay taxes to the government, no matter the reasons for it.

In games exist a few Status Quos. Some are positive, others are negative. I'll be focusing on the negative ones, why they are negative and why accepting them as the norm is not okay.

### Microtransactions

There is almost no game anymore that isn't multiplayer, while not also offering some form of ingame store to buy cosmetics or similar.

A very common practice used in these stores is the conversion of real money into virtual one used in the game's store. Especially mobile games love to do this.  
The reasons are somewhat obvious. It makes you lose track of how much money you actually used and allows the game publishers to set the worth of their stuff however they please. In addition are prices of items and the amount of currency you can buy set in such ways that you always have a remainder left, reminding you of unused money you spent, so you buy more currency to get rid of this remainder.

This is a bad practice I've seen in countless mobile games. But also PC games aren't spared from this. And finally do even Minecraft Servers (And in case of Essential also mods) adobt this practice, even tho they shouldn't.

This entire thing is accepted as a common and okay thing to do, but is not good at all.

### Forced online connections

Another bad practice many seem to take as a given are games where you expect a Singleplayer to work, but as soon as you don't have Internet, or as soon as the people behind the game pull the plug on the Servers is it no longer working.  
Prime examples are "Tony Hawks Pro Skater 5" and "The Crew". The former may still work when cutting off the Internet, while the other is completely unplayable.

Such games are a huge pain as it makes you lose a game you've paid for to play, only because the people behind it want full control and all the data they can get. This is why [StopKillingGames.com]{ target="_blank" rel="nofollow" } was created. To finally stop such an awful practice people just take as a given.

### Buggy first releases

Games these days are more and more published as a generally unfinished, buggy mess, where crucial bug fixes are made through later patches.

As an example can I give "Tony Hawks Pro Skater 5"... again. This game when bought as a Console game, was incomplete and you had to download an 8GB patch to get all the missing content.  
Critical parts such as complete levels were missing and unobtainable if you couldn't get this patch somehow.

Yet, it seems accepted by people to have such buggy games published because "They will fix it".  
It actually makes you apreciate games like Minecraft, where Mojang isn't just publishing a new release and then patching bugs later on... They actively do snapshot releases, pre-releases and release candidates to make the new update as stable as possible first before actually making a proper release.

It's understandable that these publishers want their games to be out ASAP to make money... But publishing it with major bugs that you encounter **without** provocation of any kind is not a justifiable thing and should be met with severe backlash. Yet, people seem not too interested and instead just accept it.

## Final Words

It's rather sad to see how people often use the above cases as an excuse for a game or similar doing the same. And those weren't the only one. I've also seen examples like "Other sites and softwares aren't open source either. So why should this be?".  
These things aren't excusable reasons to justify what has become a Status Quo that actively destoys gaming culture as a whole.

Games in the past had great level designs, solid gameplay and proper testing. They too had bugs, yes. But often times did you had to do unconventional things to actually get these bugs to begin with, while some games these days have you just walk and encounter a game breaking bug (Just see "Homefront: The Revolution" for some good examples).

We should stop accepting bad practices because many do it. If we don't force them to change, we eventually will have situations where they can make you lose ownership of games you bought physically in a store (If that isn't even the case already with the current nature of patching bought games... See Nintendo and their Switch.).

We can and should demand better!

[blog post]: 05-30-do-not-use-essential-mod.md
[stopkillinggames.com]: https://stopkillinggames.com

[^1]:
    "The untold TRUTH about Essential | Why you shouldn't use this mod" by NicknameHidden  
    https://www.youtube.com/watch?v=Ax1bGF5G5MM
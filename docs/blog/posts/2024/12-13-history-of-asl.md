---
title: The Development History of AdvancedServerList
description: "Sharing the history of AdvancedServerList's development as a 25k downloads special."

date: 2024-12-13

authors:
  - andre601

categories:
  - General

tags:
  - Misc

comment_id: '113642708791748598'
---

# The Development History of AdvancedServerList

Just in time for my birthday did my plugin AdvancedServerList reach 25 thousand downloads on Modrinth[^1].  
This is a number I've never expected to reach here. So as a small thank you am I sharing some snippets of the plugin's development throughout the past 3 years and how it evolved to what it is now. Enjoy.

<!-- more -->

## Some Statistics

Here are some statistics I've gathered for this post:

-   The plugin has been downloaded a total of 28,990 times across SpigotMC, Modrinth, Hangar and the Codeberg Repository (Codeberg counts possibly include migrated numbers from GitHub).
    - 2,638 downloads alone are on Codeberg, excluding BanPlugins Addon counts (9.10% of all downloads).
    - 25,061 downloads were made on Modrinth, including BanPlugins Addon counts, as there is no separate counter for those (86.45% of all downloads).[^2]
    - 1,291 downloads are on Hangar (4.45% of all downloads).
-   1,695 commits and 93 releases have been made on the Repository.
-   272 releases were made on Modrinth so far. That is on average 90.666... releases per platform (Paper, BungeeCord/Waterfall, Velocity).
    - This number is not accurate as it doesn't account for releases with 4 platforms (Spigot, Paper, BungeeCord/Waterfall, Velocity).  
-   Hangar has 55 releases, with 3 being external ones (Linking to repository releases).

I actually made a Tool in java to fetch download counts from Modrinth and Codeberg to then display in a formatted way.  
The output of this result can be seen below.
    
/// details | Download Statistics Table
    type: info

```
+----------------------------------------------------+
|                      Codeberg                      |
+---------------------------+------------------------+
| Spigot                    |                    287 |
| Velocity                  |                    480 |
| BungeeCord                |                    570 |
| Paper                     |                    619 |
| Bukkit                    |                    682 |
+---------------------------+------------------------+
| Total downloads:                             2,638 |
+====================================================+
| Total Releases:                                 93 |
|                                                    |
| Most downloaded:                                   |
|                AdvancedServerList-Bukkit-3.2.0.jar |
|                                      174 Downloads |
|                                                    |
|            AdvancedServerList-BungeeCord-5.1.2.jar |
|                                        0 downloads |
+----------------------------------------------------+



+----------------------------------------------------+
|                      Modrinth                      |
+---------------------------+------------------------+
| Spigot                    |                    901 |
| Bukkit                    |                  2,197 |
| BungeeCord                |                  4,677 |
| Velocity                  |                  5,660 |
| Paper                     |                 11,626 |
+---------------------------+------------------------+
| Total downloads:                            25,061 |
+====================================================+
| Total Releases:                                272 |
|                                                    |
| Most downloaded:                                   |
|                 AdvancedServerList-Paper-5.0.5.jar |
|                                    1,197 downloads |
|                                                    |
| Least downloaded:                                  |
|            AdvancedServerList-BungeeCord-5.1.1.jar |
|                                        8 downloads |
+----------------------------------------------------+
```
///

## The Plugin History

### Development History Video

I've made a YouTube Video of the plugins Development history using Gource[^3] to visualize it.

Click the below image to get redirected to the video.

[![thumbnail](https://img.youtube.com/vi/85IPz3bSwes/0.jpg)](https://youtu.be/85IPz3bSwes){ target="_blank" rel="nofollow" }

### A simple start

Everything started with a User on Spigot reaching out to me, asking me if they could configure another plugin of mine, OneVersionRemake[^4], in such a way that it could display a custom player count text for allowed versions.  
I first told them it's not doable, but when they asked if I could make a standalone version with such a feature did it all start... One day later the first pre-release was published on GitHub.  
On the 26th of june, 2022, my plugin was published on Spigot.

The plugin already had a lot of its core features, such as custom MOTD, player count text and player hover, but it was only the beginning...

### v1.3.0 and v1.4.0: More players...

v1.3.0 and v1.4.0 introduced options to modify the max player count. This was suggested by a user on GitHub and first started as an option called `oneMore`, setting the max player count to the online player count + 1.  
This later changed to `xMore` as the user suggested changes to allow to configure how much should be added or removed. The provided Pull request wasn't as good tho, as it had a small flaw, resulting in me making my own version of it later on.

### v1.5.0: Favicons

v1.5.0 was one of the bigger updates for me with the plugin, as it introduce support for Favicons.  
I remember being frustrated about implementing support, as each server/proxy had their own type of Favicon and didn't just use the image directly in some way.

The implementation had a small flaw tho: The cache used only cached the BufferedImage, which would then be converted to the actual Favicon used. I later changed this to caching the final Favicon instead.  
A final change to the favicon handling would much later come in the form of storing local favicons separately, to not have them go through the same process as those from a URL or player name/uuid.

### v1.6.0: Restructuring the Server List Profile

With v1.6.0 did I restructure the Server List Profile to what it is now (minus options such as profiles, maxPlayers, etc.).

### v1.8.0: One (Event Handler) for All

I wasn't happy with how every platform had its own event handling, so I spent time on creating a unified event handling instead.  
In a nutshell, when a event is received is the plugin creating a version of a specific class containing methods and instances of the event and its values. The core of the plugin would then call these methods to retrieve generic data to then further handle.
Tl;dr: Events are turned into more generic values to be handled... more or less (I'm not good at explaining this ;-;).

### v1.9.0: First attempt at multiple MOTDs

People wanted multiple MOTDs, because ServerListPlus[^5] has support for it. So I implemented that in a similar fashion through a dedicated option named `motds`.  
The structure was virtually the same of what you know from SLP.

Tho, this option didn't last long because...

### v1.10.0: The profiles update

I can remember being annoyed that I would need to make specific options for every option that should allow multiple variants, so instead I decided to make one core option containing the other ones.  
This is where `profiles` was born.  
From the start did I already make sure that it uses the options outside profiles, if none is set in the one selected.

### v1.11.0: Better Player caches

The player caching was awful and not good. From what I can recall was every entry saved as `<ip>=<name>` in a `cache.data` file.  
I decided to redo the caching, resulting in data now being stored as JSON in a `playercache.json` file. This file is still used today and has the same content cached.

### v2.0.0: Fancy new API

v2.0.0 was one of my favourite updates, as it was my first time working out an API for people to use.  
Tho, this API wasn't the best, having to maintain 2 versions (1 at the plugin and a separate one people could use), but I was still proud of managing to implement a system to have custom placeholders added.

This update also announced a move of the wiki away from GitHub and onto a GitHub Pages hosted one using MkDocs.

### v3.0.0: Paper Plugin stuff

With v3 came support for PaperMC's `paper-plugin` system, allowing a single jar for Spigot and Paper, without having pointless stuff done such as downloading libraries on a Paper server that are already included.

### v3.1.0: Breaking Paper Plugin changes

With v3.1.0 did I had to announce that older Paper versions aren't supported anymore, as Paper added breaking changes to their `paper-plugin.yml` file, making dependencies no longer work reliably.  
This update however, also brought the introduction of a ConfigMigrator, allowing me to update the config.yml of the plugin without losing its set options.

### v3.3.0: PAPIProxyBridge support and FaviconHandler improvements

In v3.3.0 did I implement support for PAPIProxyBridge[^6] but more importantly improvements to the FaviconHandler.  
Until that point were favicons made synchronously, meaning on the main server/proxy thread. This caused unneeded delays for players when they ping the server, making it look like it had bad ping. With this update was the handling changed to be asynchronous. Tho, this introduced the issue that favicons made not be fully created yet when the plugin needs them for the event, so they aren't displayed properly.

### v3.4.0: The move to Codeberg.org

With this version did I announce support for the Maintenance[^7] plugin, but also my move of the code from GitHub.com over to Codeberg.org.  
There were multiple reasons for it, but the most important one was, that I wasn't too happy with some of GitHub's choices.

This also introduced a third, but final, move of the wiki to the location it can be found now.

### v3.5.0: maxPlayers option

I added the `maxPlayers` option in v3.5.0 as an alternative to the `extraPlayers` option, while also having the API updated with some new options and deprecations.

### v3.6.0: Bye bye Spigot

v3.6.0 marked the move away from Spigot, making my plugin a Paper-plugin only solution for servers.  
This was primarely done because I was sick of having to rely on a 3rd-party plugin - ProtocolLib[^8] - while Paper just offers an event right out of the box. It didn't make sense to keep supporting this platform, having to go out of my way to implement necessary stuff like libraries already present in the competition.

### v3.7.0: Much improved Condition System

The old condition system was quite awful and limited in its functionality.  
So after starting to understand the system behind BungeeTabListPlus'[^9] condition handling did I implement it into my plugin too. This change allowed so much more to be done for people and I was proud about having it implemented into my stuff.

The update also moved the update checker to use the HTTPClient available in Java, making the Paper version no longer having to download OkHttp as a library to use.

Finally were also 3 new Operands added to check if the left string starts with the right string (`|-`), ends with it (`-|`) or contains it (`<-`). The last one was later changed to `<_` to avoid possible parsing issues with negative numbers.

### v4.0.0: Goodbye conditions

The old `conditions` option was removed as the newer and more powerful `condition` option was in place for a while.

It also brought a critical bugfix for placeholders in conditions while also adding the very first 1st-party addon for AdvancedServerList: The BanPlugins Addon.

### v4.1.0: Faster local favicons

Until this version did local favicons go through the same system as ones from URL or player name/uuid. This included the same issue of taking a while to be available.  
Given these favicons are static and shouldn't change at all was the system changed to load them at startup and have them cached indefinitely. That way could they be returned instantly.

### v4.3.0: Separated ExpressionParser

The expression parser was separated into its own library[^10] for others to use, with the plugin now shading it in.

### v4.6.2: Fixing wrong protocol for placeholder

This version was embarassing for me, as it fixed an oversight nobody had noticed for quite a while, where `${player protocol}` returned the Protocol of the server, rather than the player.  
I also made a clarification on BungeeCord/Waterfall support, which I announced in a previous release to be on a "maintenance only" state, fixing smaller bugs but not attempting to fix breaking changes.

### v4.8.0: The ServerListPlus migrator and thanks Mojank

This update was a big one for me, as it implemented a migrator to convert your ServerListPlus configuration into AdvancedServerList Server List Profiles.  
Downside of it was and still is, that SLP needs to be present, as the configuration can't be read by ASL's configuration library and therefore requires usage of SLP's internal API.

Another major thing was a breaking change introduced by Paper, forwarded from Mojang.  
Player names could no longer be more than 16 characters. This had an effect on the player hover text as it - for whatever reason - uses Player profiles, meaning this limit applied.  
This change forced me to limit text to 16 characters. I managed to change this later to use a workaround by having my own custom profiles made, bypassing the 16 character limit.

### v4.10.0: MiniMOTD migrator

MiniMOTD[^11] migration support was added later on, which unlike SLP didn't need the plugin to be present, but did need the download and usage of a library.

### v4.11.0: Better Commands

The command system was reworked, adding hower text and click actions alongside a new `/asl profiles` subcommand to create, copy or list profiles.

### v4.12.0: Thanks Paper

Paper made a sort of fix for the hover issue mentioned earler... Issue was that this broke the old system and it wasn't announced at all, having me be confused as to why it didn't work anymore.  
I eventually implemented a fix for this.

### v5.0.0: Online Players option and more

v5 added a `onlinePlayers` option that is similar in structure to the `extraPlayers` or `maxPlayers` option.  
Another major change was that the aforementioned options were changed from only allowing a number to allowing Strings, meaning you could now use number placeholders to more dynamically change the output.

Finally was a `${proxy ...}` placeholder added, allowing to display content from backend-servers if they were available.

### v5.1.0: Random Favicons

The Favicon option received a `random` option support, as a user wanted to have a random favicon alongside randomized MOTDs, which with the profiles system wasn't really doable.

## Plans for the future

Now the question: What are future plans?  
Right now, there isn't really any big goals I have for the plugin. I want to keep maintaining it for as long as I can, while also improving it.

Tho, I did had a few Ideas in mind that I may or may not look into:

- `Sets`: A option where you can define different values for motds, player counts, etc. that could be referenced in any of the profile options with a syntax... Maybe like `$set:<setname>`. This would basically allow randomized combinations of outputs that don't have the flaws of the profiles option (Only pairs of options).
- Proper Unit tests: The only test right now is for the FelxVer comparitor, which isn't much of a test anyways. So proper unit testing would be a useful thing for the future. I unfortunately have no knowledge in this, so help on this is wanted.

[^1]: https://modrinth.com/plugin/advancedserverlist
[^2]: Number is based on the total number of downloads from all releases collected on Modrinth and actually differs from the value reported by the API which is less, as it most likely doesn't count downloads of non-primary Jar files.
[^3]: https://gource.io
[^4]: https://modrinth.com/plugin/oneversionremake
[^5]: https://github.com/Minecrell/ServerListPlus
[^6]: https://modrinth.com/plugin/papiproxybridge
[^7]: https://modrinth.com/plugin/maintenance
[^8]: https://github.com/dmulloy2/ProtocolLib
[^9]: https://github.com/CodeCrafter47/BungeeTabListPlus
[^10]: https://codeberg.org/Andre601/ExpressionParser
[^11]: https://modrinth.com/plugin/minimotd
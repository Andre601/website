---
title: "How to add the 'fediverse:creator' meta tag for blog pages in MkDocs"
description: "A quick tutorial on how you can add the 'fediverse:creator' meta tag to your blog posts in MkDocs to show your Mastodon account on links on Mastodon."

date: 2024-11-14

authors:
  - andre601

categories:
  - Guide

tags:
  - Tutorial
  - Mastodon

comment_id: '113483103576142118'

links:
  - blog/posts/2022/11-18-mastodon.md
---

# How to add the `fediverse:creator` meta tag for blog posts in MkDocs

Mastodon 4.3 added a feature, where links to sites can display an author affilialtion, linking to a Mastodon account associated with that site.  
This feature works through the usage of a `meta` HTML tag containing a specific name and content value.

This small blog post explains how you can add this tag to Blog posts of your MkDocs site.

<!-- more -->

## Before Starting

Please be aware of the following points regarding this blog post:

-   I will only explain how to add the Meta tag to blog posts, not the whole site itself, as this can easily be done via basic theme extending.
-   This post assumes you use the Material for MkDocs[^1] Theme and its Blog Plugin[^2].
-   This post assumes basic understanding of extending the Material for MkDocs theme. Please see "Extending the theme"[^3] in the Customization page of Material for MkDocs.
-   The Username and Instance will be set in the `config.yml` of MkDocs like so:  
    ```yaml
    extra:
      mastodon:
        user: exampleuser
        host: example.com
    ```  
    This makes it easier to update later on, and can also be used for other things like [implementing a comment system](../2023/07-23-setup-mastodon-comment-system.md){ target="_blank" rel="nofollow" }.

## How does it work?

The system is similar to how you can validate URLs in your Mastodon Profile. However, instead of a `rel="me"` attribute in an `a` html tag, which could be in any part of the website, is a `meta` tag used that has `fediverse:creator` as its name attribute and `@<username>@<instance>` as its content attribute, and is put in the site's `head` part.  
Here is a small example of this Meta tag:  

```html { title="example.html" }
<head>
  <!-- Other (Meta) Tags -->
  <meta name="fediverse:creator" content="@example@example.com">
</head>
```

Similar to the link verification is part of this setup to add the domain(s) that should be associated with your Account to your "Author Attribution" section in your Verification Settings (Under `Public Profile` in your settings).  
Once you've added the domains and the site has the meta tag added will any link pointing to a page under that domain containing this tag have a ":simple-mastodon: More from &lt;Username&gt;" appended to the link preview.

Here is an example:  
![author-attribution-example](../../../assets/img/posts/add-fediverse-creator-tag/author-attribution-example.png)

## Overriding Theme files

In order for us to add the meta tag to Blog Posts and nothing else, can we try one of two possible aproaches. Each has their pros and cons which I will cover.

### Overriding `main.html`

Overriding `main.html` is a bit more tedious to do as it requires more checks to be made to ensure only blog posts are affected.  
However, the benefit is, that extending it is somewhat simpler, as it doesn't contain any blocks by default and only extends `base.html`, meaning you don't need to copy over entire parts of the original file.

It is recommended to put the meta tag in the `extrahead` block, which also allows other meta tags to be included.  
In order for us to only add this tag to blog posts, are we required to check the `page.meta.template` value for `blog-post.html`, as that is the template Material for MkDocs' blog plugin uses for Blog posts:

```html {title="main.html" .annotate}
{% extends "base.html" %}

{% block extrahead %}
  <!-- (1) -->
  {% if page.meta.template == "blog-post.html" %}
    <!-- (2) -->
    {% if config.extra and config.extra.mastodon and config.extra.mastodon.user and config.extra.mastodon.host %}
      <meta name="fediverse:creator" content="@{{ config.extra.mastodon.user }}@{{ config.extra.mastodon.host }}">
    {% endif %}
  {% endif %}
{% endblock %}
```

1.  This checks on wether the page itself is using the `blog-post.html` template. This makes sure that only blog posts have this tag.  
    If you want all pages to have this tag can you remove this if-check.
2.  This checks for the existance of the `extra` option in the `config.yml` of MkDocs and wether it also conatins the `mastodon -> user` and `mastodon -> host` options.  
    If you want to directly set the name in the tag can you remove this if-check and update the meta tag accordingly.

### Overriding `blog-post.html`

Overriding the `blog-post.html` file may be an easier aproach, as it removes the need to first check wether the current page is using this template.  
However, one major downside is, that you have to copy over the full `blog-post.html` content from its source[^4]. This is because of the fact that the template overrides the `container` block of its parent file, `main.html`, and not including this override would result in content not being displayed properly.

With all this said, given that this template file extends `main.html` can we just use the `extrahead` block as we did in the previous section with the difference that we aren't required to check for the template type:

```html {title="blog-post.html" .annotate}
{% extends "main.html" %}
{% import "partials/nav-item.html" as item with context %}

{% block extrahead %}
  {{ super() }} <!-- (1) -->
  
  <!-- (2) -->
  {% if config.extra and config.extra.mastodon and config.extra.mastodon.user and config.extra.mastodon.host %}
    <meta name="fediverse:creator" content="@{{ config.extra.mastodon.user }}@{{ config.extra.mastodon.host }}">
  {% endif %}
{% endblock %}
  
{% block container %}
  <!-- (3) -->
{% endblock %}
```

1.  We use `{{ super() }}` here to include any content from the parent file.  
    This is useful in cases where you override the `main.html` file (i.e. to add more meta tags) and want them be part of the blog post itself.
2.  This checks for the existance of the `extra` option in the `config.yml` of MkDocs and wether it also conatins the `mastodon -> user` and `mastodon -> host` options.  
    If you want to directly set the name in the tag can you remove this if-check and update the meta tag accordingly.
3.  Content omitted for readability. Please see [Footnote #4](#fn:4) for the source and its full content.

## You're done!

This was already it. The only thing left to do is publish your site and update the "Author Affiliation" setting to include the domain(s) your site uses.  
Now, any future post containing links to your blog posts should add a link to your Mastodon account.

[^1]: https://github.com/squidfunk/mkdocs-material
[^2]: https://squidfunk.github.io/mkdocs-material/plugins/blog/
[^3]: https://squidfunk.github.io/mkdocs-material/customization/#extending-the-theme
[^4]: https://github.com/squidfunk/mkdocs-material/blob/0fc36a1f4c90f350be7c51f5600d7f33716faa7b/material/templates/blog-post.html
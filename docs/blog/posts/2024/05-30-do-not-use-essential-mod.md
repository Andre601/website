---
title: Do not use the Essential Mod
description: Reasons why you should not use the Essential Mod.

date: 2024-05-30

authors:
  - andre601

categories:
  - Critique

tags:
  - Minecraft

comment_id: '112555598323161392'
---

# Do not use the Essential Mod

The Essential Mod[^1], not to be confused with the Essentials Plugin[^2] or its updated Fork EssentialsX[^3], is a mod that tries to be a all-in-one solution for players similar to what you imagine with Mods like LabyMod[^4] or the 5zig Reborn[^5] Mod.  
The issue here is, that it has a lot of problems, making it a worse mod and not really a good choice to begin with. This post will cover all the points I managed to gather from the community over the years I've heard of this mod. Enjoy.

<!-- more -->

## The issues

As mentioned are there various issues with this mod that don't make it a good choice, if you actually care about those.

### Closed Source

The first issue this mod has, and that it shares with other bad mods such as OptiFine[^6] is its closed-source nature.  
The code of this mod is not freely available to look into, making it harder to understand, what is going on behind the scenese. This also makes it problematic for mod developers to figure out what the mod is changing in terms of code, to adapt their mods accordingly to avoid issues, which brings us straight into the next issue...

### Mod compat issues

Essential regularely causes issues with mod compatability, which is a behaviour it also shares with the OptiFine mod.  
This is mainly due to its closed-source nature, making it hard if not impossible to figure out what is happening to make your mod work with it.

### Shipping download as installer

Unlike many other mods does Essential not provide a mod jar you can download and then add to your pre-existing mod-profile. Instead you have to download and execute an exe file that installs a profile onto your Minecraft Launcher.  
While this may not be that big of a deal - after all are mods such as LabyMod or OptiFine (Yet again) also shipping an exe - is there still the fact, that you can't really know *what exactly* is being installed when executing this file.

Best-case-scenario: The mod itself is just being installed. Worst-case-scenario: Viruses, trackers, etc are being added to steal/collect (sensitive) information from your PC to share with someone.  
Of course, it most likely won't be the worst-case-scenario here, but when a Mod ships as an exe, rather than a jar you can add to other mods, should you be a bit cautious.

One final argument is its downloads on Modrinth[^7] which are jar files. And while one could assume this to be the Mod jar, is this not the case. Instead is this a jar that only has the purpose of downloading the actual Essential Mod during startup and force it into the Modloader to be loaded

### Paid Cosmetics System

The mod brags about infinite character customization with their countless cosmetics. However, what they don't share is the fact, that not all cosmetics are free, but require a premium currency called "Essential Coins", meaning if you really want this one cute looking (cuteness debatable) hat, you may need to pay some real life money to get it.  
Given that the mod seems to be made by what can only be assumed to be a for-profit company - ModCore Inc. - It's no surprise they have a premium currency system in place to make money (Disclaimer: I don't want to say that mod creators can't make revenue that way, but having a separate currency instead of direct purchase options feels like a sales-tactic from shady online-games).

## The tl;dr

The Essential Mod is effectively combining all the worst practices from other mods that exist already:

- It is closed source similar to OptiFine and LabyMod.
- It breaks mod compat similar to OptiFine.
- It comes as an installer rather than a jar you can add to an already existing Launcher Profile, similar to Optifine and LabyMod.
- It has a premium currency system one usually only sees in Minecraft Bedrock Edition, some Free2Play Mobile App, or LabyMod (You notice a pattern yet?). And finally...
- Instead of trying to do one thing really good is it trying to be yet another "all-in-one" mod solution that we have seen over the years.

## What alternatives are there?

Pretty much any other open-source mod available on Modrinth or even CurseForge[^8] is a better alternative than relying on and using a closed-source mod solution.  
@Blurryface[^9] made the site notessential.blurryface.xyz[^10] with a dedicated page listing alternatives you can use for features of the Essentials mod, so go and check it out.

## Final Words

I hope this post helps in understanding why the Essential Mod isn't as good as it claims to be. If you're still wanting to use the mod, go for it. I won't stop you in your choices. But just remember that there are a lot of alternatives available, that offer the features you may want to use for free.

## Special Thanks

- @Blurryface for their notessential page, giving me the idea of making this post.
- @Gaming32 for corrections and the additional info regarding the Essential Jar from Modrinth.

[^1]: https://essential.gg
[^2]: https://dev.bukkit.org/projects/essentials
[^3]: https://essentialsx.net
[^4]: https://labymod.net
[^5]: https://5zigreborn.eu
[^6]: https://optifine.net/home
[^7]: https://modrinth.com
[^8]: https://curseforge.com
[^9]: 
    Website: https://blurryface.xyz/  
    Mastodon: https://tech.lgbt/@blurryface
[^10]: https://notessential.blurryface.xyz/
<a href="https://squidfunk.github.io/mkdocs-material" target="_blank" rel="nofollow">
  <img src="https://cdn.jsdelivr.net/gh/Andre601/devins-badges@v3.x-mkdocs-material/assets/cozy/built-with/mkdocs-material_vector.svg" align="right" height="64" alt="mkdocs-material", title="Built with Material for MkDocs">
</a>
<a href="https://www.mkdocs.org" target="_blank" rel="nofollow">
  <img src="https://cdn.jsdelivr.net/npm/@intergrav/devins-badges@3/assets/cozy/built-with/mkdocs_vector.svg" align="right" height="64" alt="mkdocs" title="Blog built with MkDocs">
</a>

# Personal Website

This repository contains all the content used for my [personal website][website].

[Forgejo Actions][forgejo] is being used to build the website, before pushing it either to the [`pages`][pages-repo] or the [`preview`][preview-repo] Repository, depending on if it is a commit to main branch or a Pull request.  
[Codeberg Pages][pages] is used to actually display the website.

## Credits

Site is build using [MkDocs][mkdocs] and the [Material for MkDocs][theme] theme.

[website]: https://andre601.ch

[forgejo]: https://forgejo.org

[pages-repo]: https://codeberg.org/Andre601/pages
[preview-repo]: https://codeberg.org/Andre601/preview

[pages]: https://codeberg.page

[mkdocs]: https://www.mkdocs.org
[theme]: https://squidfunk.github.io/mkdocs-material